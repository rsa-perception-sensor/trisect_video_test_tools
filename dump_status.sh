#!/usr/bin/bash

sudo dmesg > dmesg.txt

if [[ -e /dev/video0 ]]; then
    v4l2-ctl -d 0 --all 2>&1 > v4l2-ctl-video0.txt
fi

if [[ -e /dev/video1 ]]; then
    v4l2-ctl -d 1 --all 2>&1 > v4l2-ctl-video1.txt
fi
