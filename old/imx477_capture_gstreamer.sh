#!/bin/sh

#SENSOR_ID=1
NUM_BUFFERS=2
FRAMERATE=2



gst-launch-1.0 -e nvarguscamerasrc num-buffers=$NUM_BUFFERS sensor-id=$SENSOR_ID ! "video/x-raw(memory:NVMM),width=4032,height=3040,framerate=$FRAMERATE/1" ! nvjpegenc ! multifilesink location=%03d_rpi_v3_imx477_cam0.jpeg
