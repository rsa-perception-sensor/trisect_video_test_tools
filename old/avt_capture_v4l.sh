#!/bin/sh

#SENSOR_ID=0
NUM_BUFFERS=2
FRAMERATE=2

echo "Capturing sensor $SENSOR_ID"
v4l2-ctl -d $SENSOR_ID -p $FRAMERATE
v4l2-ctl -V -d $SENSOR_ID  --set-fmt-video=width=1920,height=1216,pixelformat=GREY --set-ctrl bypass_mode=0 --stream-mmap --stream-to=file.raw

#gst-launch-1.0 -e nvarguscamerasrc num-buffers=$NUM_BUFFERS sensor-id=$SENSOR_ID ! "video/x-raw(memory:NVMM),width=4032,height=3040,framerate=$FRAMERATE/1" ! nvjpegenc ! multifilesink location=%03d_rpi_v3_imx477_cam0.jpeg
