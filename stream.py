#!/usr/bin/env python3

import argparse
from pathlib import Path
from subprocess import Popen, PIPE, STDOUT
import re


def valid_video_device(device: str) -> Path:
    """Custom argparse type for /dev/video? devices"""

    if "/" in device:
        pd = Path(device)
    else:
        pd = Path("/dev/") / device

    if not pd.exists():
        msg = "Device ({}) does not exist".format(pd)
        raise argparse.ArgumentTypeError(msg)

    elif not pd.is_char_device():
        msg = "Device ({}) does not appear to be a char device".format(pd)
        raise argparse.ArgumentTypeError(msg)

    return pd


def v4l_syspath(devpath):
    return (
        Path("/sys/devices/platform/tegra-capture-vi/video4linux") / Path(devpath).stem
    )


# ===================================================================
#
# Cameras


class Camera:
    def __init__(self, device):
        self.device = device

    def v4l_syspath(self):
        return v4l_syspath(self.device)

    @property
    def devnum(self):
        return int(re.search(r"\d+", str(self.device)).group())


class IMX477(Camera):
    def __init__(self, device):
        Camera.__init__(self, device)

        self.height = 2160
        self.width = 3840
        self.format = "RG10"  # "JXR0"
        self.gst_source = "nvarguscamerasrc sensor-id=%d silent=false" % self.devnum


class AVTCamera(Camera):
    def __init__(self, device):
        Camera.__init__(self, device)

        self.height = 1216
        self.width = 1920
        self.format = "GREY"

        self.gst_source = None


# convert -size 1920x1216 -depth 8 gray:frame.raw frame.jpg


# ===================================================================
#
# Streamers


class V4LStreamer:
    def __init__(self, args, camera):
        self.camera = camera
        self.args = args

    def run(self):

        cmd = (
            "v4l2-ctl -V -d %s --set-fmt-video=width=%d,height=%d,pixelformat=%s --set-ctrl bypass_mode=0 --stream-mmap --stream-to=frame.raw --stream-count=1"
            % (
                self.camera.device,
                self.camera.width,
                self.camera.height,
                self.camera.format,
            )
        )
        cmd = cmd.split(" ")

        if self.args.framerate:
            print("Setting framerate to %f" % self.args.framerate)
            cmd.extend(["-p", "%f" % self.args.framerate])

        print("Running: " + " ".join(cmd))
        print("!! ctrl-c to stop")

        with Popen(cmd, stdout=PIPE, bufsize=1, universal_newlines=True) as p:
            for line in p.stdout:
                print(line, end="")


class GSTStreamer:
    def __init__(self, args, camera):
        self.camera = camera
        self.args = args

    def run(self):

        if not self.camera.gst_source:
            print("Cannot run this sensor with GStreamer")
            return

        framerate = 5
        if self.args.framerate:
            framerate = round(self.args.framerate)

        cmd = (
            'GST_DEBUG=1 gst-launch-1.0 -v -e %s num-buffers=2 ! "video/x-raw(memory:NVMM),width=%d,height=%d,framerate=%d/1" ! nvjpegenc ! multifilesink location=%%03d_rpi_v3_imx477_cam0.jpeg'
            % (
                self.camera.gst_source,
                self.camera.width,
                self.camera.height,
                # self.camera.format,
                framerate,
            )
        )
        # cmd = cmd.split(" ")

        print("Running: " + cmd)
        print("!! ctrl-c to stop")

        with Popen(
            cmd,
            shell=True,
            stdout=PIPE,
            stderr=STDOUT,
            bufsize=1,
            universal_newlines=True,
        ) as p:
            for line in p.stdout:
                print(line, end="")

        pass


# ===================================================================
#
# main()


def main():

    parser = argparse.ArgumentParser(
        prog="stream.py", description="Wrapper around Jetson streaming"
    )

    parser.add_argument(
        "device", type=valid_video_device, help="/dev/... entry to stream"
    )

    parser.add_argument("--framerate", type=float, help="Desired frame rate in fps")

    access_group = parser.add_mutually_exclusive_group(required=True)
    access_group.add_argument(
        "--v4l", action="store_true", help="Use V4L (mutually exclusive with --gst)"
    )

    access_group.add_argument(
        "--gst",
        action="store_true",
        help="Use gstreamer (mutually exclusive with --v4l)",
    )

    # output_group = parser.add_mutually_exclusive_group(required=True)
    # output_group.add_argument(
    #     "--screen", action="store_true", help="Stream to screen (with --gst)"
    # )

    # output_group.add_argument(
    #     "--save", action="store_true", help="Save to file (with --gst and --v4l)"
    # )

    args = parser.parse_args()

    print("Opening device %s" % args.device)

    # Auto detect the sensor type associated with the /dev
    syspath = v4l_syspath(args.device) / "name"

    model_name = open(syspath).readline()

    print("Video device %s is a %s" % (args.device, model_name))

    if "imx477" in model_name:
        camera = IMX477(args.device)
    elif "C-240" in model_name:
        camera = AVTCamera(args.device)
    else:
        msg = 'Device %s is model "%s", which I can\'t handle' % (
            args.device,
            model_name,
        )
        raise Exception(msg)

    if args.v4l:
        V4LStreamer(args, camera).run()
    elif args.gst:
        GSTStreamer(args, camera).run()


if __name__ == "__main__":
    main()
